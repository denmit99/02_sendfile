import java.io.*;
import java.net.ConnectException;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

public class Client {

    private static String FILEPATH;
    private static int SERVERPORT;
    private static String SERVERIP;
    private static int BUFSIZE = 1024;

    public static void main(String[] args) {

        try {
            FILEPATH = args[0];
            SERVERIP = args[1];
            SERVERPORT = Integer.valueOf(args[2]);

            File file = new File(FILEPATH);
            byte[] buffer = new byte[BUFSIZE];
            Socket socket = new Socket(SERVERIP, SERVERPORT);
            FileInputStream reader = new FileInputStream(file);
            DataOutputStream out = new DataOutputStream(socket.getOutputStream());
            DataInputStream in = new DataInputStream(socket.getInputStream());
            try {
                out.writeUTF(file.getName());
                out.writeUTF(Long.toString(file.length()));

                int readenBytes = -1;

                while ((readenBytes = reader.read(buffer)) != -1) {
                    out.write(buffer, 0, readenBytes);
                    out.flush();
                }
                System.out.println(in.readUTF());
            } catch (SocketException e) {
                e.printStackTrace();
                System.out.println("Socket was closed");
            }
            in.close();
            out.close();
            reader.close();
        } catch (UnknownHostException e) {
            e.printStackTrace();
            System.out.println("Host " + SERVERIP + ":" + SERVERPORT + " is unknown.");
        } catch (ConnectException e) {
            e.printStackTrace();
            System.out.println("Error while connecting to server");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}