import java.io.*;
import java.net.Socket;

public class ClientHandler implements Runnable {

    private static Socket client;
    private static final int BUFSIZE = 1024;
    private static final int SPEEDTIMEOUT = 3000;
    private static String clientIP;

    public ClientHandler(Socket s) {
        client = s;
    }

    @Override
    public void run() {
        try {
            clientIP = client.getInetAddress().toString();
            DataOutputStream out = new DataOutputStream(client.getOutputStream());
            DataInputStream in = new DataInputStream(client.getInputStream());

            String fileName = in.readUTF();
            if (isCorrectFilePath(fileName)) {
                long expectedFileSize = Long.parseLong(in.readUTF());
                long actualFileSize = 0;

                System.out.println("Filename is " + fileName);
                System.out.println("Expected filesize is " + expectedFileSize);

                File newFile = new File("/home/denmit99/IdeaProjects/seti2/src/main/java/uploads/" + fileName); //ИСПРАВИТЬ ПУТЬ
                if (newFile.createNewFile())
                    System.out.println("File " + fileName + " was created.");
                else {
                    System.out.println("File " + fileName + " wasn't created");
                    client.close();
                    return;
                }

                FileOutputStream writer = new FileOutputStream(newFile, false);

                byte[] buffer = new byte[BUFSIZE];
                int readenBytes = 0, speedIteration = 0;
                long instTime = 1, currentTime, averageTime = 1, instBytes = 0, previousTime = System.currentTimeMillis();
                long startTime = System.currentTimeMillis();

                while ((actualFileSize < expectedFileSize) && (!client.isClosed())) {
                    readenBytes = in.read(buffer);
                    writer.write(buffer, 0, readenBytes);
                    actualFileSize += readenBytes;
                    instBytes += readenBytes;
                    currentTime = System.currentTimeMillis();
                    if ((averageTime = currentTime - startTime) == 0)
                        averageTime = 1;
                    if (averageTime > SPEEDTIMEOUT * speedIteration) {
                        System.out.println("[" + clientIP + "]..........[" + fileName + "]");
                        if ((instTime = currentTime - previousTime) == 0)
                            instTime = 1;
                        System.out.print("Aver: " + (actualFileSize * (double) 1000) / (averageTime * (double) (1024 * 1024)) + " Mbyte/s | ");
                        System.out.println("Cur: " + (instBytes * (double) 1000) / (instTime * (double) (1024 * 1024)) + " Mbyte/s");
                        System.out.println("====================");
                        previousTime = currentTime;
                        instBytes = 0;
                        speedIteration++;
                    }
                }
                System.out.println("Average speed: " + (actualFileSize * (double) 1000) / (averageTime * (double) (1024 * 1024)) + " Mbyte/s");
                System.out.println("ACTUAL SIZE: " + newFile.length() + ", EXPECTED: " + expectedFileSize);

                if (!client.isClosed()) {
                    if (newFile.length() == expectedFileSize) {
                        System.out.println("File downloading completed succesfully!\n");
                        out.writeUTF("SUCCESS");
                    } else {
                        System.out.println("File downloading completed with failure! Deleting wrong file...\n");
                        out.writeUTF("FAILURE");
                        if (!newFile.delete())
                            System.out.println("Failure while deleting file");
                    }
                }
                writer.close();
            } else {
                System.out.println("filename is incorrect!");
                client.close();
            }
            //client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    boolean isCorrectFilePath(String filepath) {
        return filepath.matches("[\\w]+[.][\\w]+");
    }
}
